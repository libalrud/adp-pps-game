import sys

debug_mode = True


def eprint(*args, **kwargs):
    """Print to STDERR"""
    print(*args, file=sys.stderr, **kwargs)


def dprint(*args, **kwargs):
    """Print to stdout conditionally"""
    if debug_mode:
        print(*args, **kwargs)
