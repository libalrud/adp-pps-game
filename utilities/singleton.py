from abc import ABC


class SingletonMeta(type):
    """Singleton implementation through metaclass

    See https://python.plainenglish.io/better-python-singleton-with-a-metaclass-41fb8bfe2127
    or https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonMeta, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Singleton(metaclass=SingletonMeta):
    """Simplification of a metaclass to an abstract base class"""
    pass
