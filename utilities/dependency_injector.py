
class DependencyInjector:
    """
    Registers and administers dependencies
    Project-wide "which alternative was used" dependency manager
    TODO implement, on standby for now
    """

    # see IDatabaseImplementor's constructor
    # IDatabaseOperator's constructor

    def __init__(self):
        self.dependencies = {}

    def register_dependency(self, subtype: type, main_type: type):
        """
        Register_dependency needs to accept a subtype of a type to be used
        """
        self.dependencies[main_type] = subtype

    def resolve_dependency(self, main_type: type):
        """
        Resolve_dependency, return class to be instantiated
        """
        return self.dependencies[main_type]
