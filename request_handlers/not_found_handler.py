from http import HTTPStatus

from request_handlers.handlers import BaseHandler
from request_routing.basic_responses import TextResponse
from request_routing.request_command import RequestCommand
from request_routing.request_response import RequestResponse


class NotFoundHandler(BaseHandler):
    def match(self, path: str) -> bool:
        return True

    def execute(self, request_command: RequestCommand) -> RequestResponse:
        content_str = self.not_found(request_command.path_info)
        return TextResponse(content_str=content_str, http_status=HTTPStatus.NOT_FOUND)

    @staticmethod
    def not_found(path_info: str) -> str:
        return f"404 not found: {path_info}"
