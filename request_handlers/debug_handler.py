from request_handlers.handlers import StandardHandler
from request_routing.basic_responses import TextResponse
from request_routing.request_command import RequestCommand
from request_routing.request_response import RequestResponse


class DebugHandler(StandardHandler):
    """A selection of Default handlers -- debug and 404 not found"""

    pattern = "debug"

    def execute(self, request_command: RequestCommand) -> RequestResponse:
        content_str = self.debug(request_command.request_method, request_command.path_info, request_command.query_info)
        return TextResponse(content_str=content_str)

    @staticmethod
    def debug(request_method: str, path_info: str, query_info: str) -> str:
        return f"{request_method=}, {path_info=}, {query_info=}"
