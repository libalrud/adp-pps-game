from abc import ABC, abstractmethod

from request_routing.request_command import RequestCommand
from request_routing.request_response import RequestResponse
from request_routing.request_router import HandlerException


class BaseHandler(ABC):
    """
    Boilerplate code for all Handlers.
    These are automatically instantiated, managed, and called by request_router
    """

    def fail(self):
        """Call this when the handler fails to generate a response ("give it to the next guy")"""
        raise HandlerException

    @abstractmethod
    def match(self, path: str) -> bool:
        """
        If true, this objects' execute method will be selected
        to process the request with the given path
        A simplified version is pre-implemented in StandardHandler
        """

    @abstractmethod
    def execute(self, request_command: RequestCommand) -> RequestResponse:
        """
        Process the request according to the given request method (GET/POST),
        path info (omitted prefix), query info (what's after the '?' -- already parsed into dict)
        Return a tuple of: status, content type, content (as defined in WSGI)
        Throw a request_router.HandlerException if the given request is not processable
        """


class StandardHandler(BaseHandler, ABC):
    """
    Implements default match method
    As string comparison to first part of URL (before first '/')
    """

    # override this!!
    pattern = None

    def match(self, path: str):

        if len(path) == 0:
            return len(self.pattern) == 0

        if path[0] == '/':
            path = path[1:]

        discriminator = path.split('/')[0]
        return discriminator == self.pattern
