from typing import Callable

from database.bridge_database import IDatabaseImplementor


class MockDatabase(IDatabaseImplementor):
    """Mocks a database with some simple data"""
    # TODO

    def __init__(self):
        pass

    def get_all_instances(self, entity_str: str) -> list:
        pass

    def get_instances(self, entity_str: str, filter_func: Callable[[any], bool], max_count: int):
        pass

    def get_instance_by_id(self, entity_str: str, obj_id: int):
        pass

    def has_id(self, entity_str: str, obj_id: int) -> bool:
        pass

    def new_instance(self, entity_str: str, **kwargs):
        pass

    def del_instance(self, entity_str: str, obj_id: int) -> bool:
        pass
