import datetime

import secret
from database.bridge_database import IDatabaseImplementor
from pony.orm import *

from database.factory_repository import RepositoryFactory
from database.ssh_tunnel import ISshTunnelManager
from utilities.debug_mode import dprint, debug_mode


# PyCharm has a bug that incorrectly highlights pony syntax
# https://youtrack.jetbrains.com/issue/PY-35584
# noinspection PyTypeChecker
class MySQLDatabase(IDatabaseImplementor):
    """Implements the common-for-any-database logic for MySQL with ponyORM"""

    db = Database()

    class Cipher(db.Entity):
        id = PrimaryKey(int, auto=True)
        name = Required(str, unique=True, max_len=45)  # todo max 45 chars
        operations = Required(str)
        ownerID = Optional(int)

    class CustomGame(db.Entity):
        _table_ = "customGame"
        id = PrimaryKey(int, auto=True)
        name = Required(str, unique=True)
        timeLimit = Required(int, default=7320)  # todo default 7320
        numlocations = Required(int, default=15)  # todo default 15
        shuffleLocations = Required(bool, default=False)  # todo default false
        seed = Optional(str, max_len=45)
        ownerID = Optional(int)

    class GameLocations(db.Entity):
        _table_ = "gameLocations"
        id = PrimaryKey(int, auto=True)
        locationID = Required(int)
        gameID = Required(int)
        cipherText = Optional(str, max_len=255)
        spell = Optional(str, max_len=45)
        solvedDescription = Optional(str)
        solvedImage = Optional(int)

    class Image(db.Entity):
        id = PrimaryKey(int, auto=True)
        name = Required(str, unique=True, max_len=200)
        imageSource = Required(str, max_len=200)
        ownerID = Optional(int)

    class Location(db.Entity):
        id = PrimaryKey(int, auto=True)
        name = Required(str, unique=True, max_len=255)
        x = Required(float)
        y = Required(float)
        description = Optional(str)
        isGameLocation = Required(bool)
        isToApprove = Required(bool)
        ownerID = Optional(int)

    class ResultEntry(db.Entity):
        _table_ = "resultEntry"
        id = PrimaryKey(int, auto=True)
        ownerID = Required(int)
        category = Required(str, max_len=1)
        gamemode = Required(str)
        customGameID = Optional(int)
        seed = Optional(str, max_len=45)
        totalPoints = Optional(int)
        gainedPoints = Optional(int)
        time = Optional(int)
        timePenalty = Optional(int)
        distanceToEnd = Optional(int)
        distancePenalty = Optional(int)
        wrongTries = Optional(int)
        wrongTriesPenalty = Optional(int)
        distanceTraveled = Optional(int)
        datetime = Required(datetime.datetime, sql_default='CURRENT_TIMESTAMP')

    class Spook(db.Entity):
        id = PrimaryKey(int, auto=True)
        name = Required(str, unique=True, max_len=60)
        description = Required(str)
        literarySource = Required(str, max_len=200)
        imageID = Optional(int)
        ownerID = Optional(int)

    class User(db.Entity):
        id = PrimaryKey(int, auto=True)
        name = Required(str, unique=True)
        password = Required(bytes)

    def __init__(self, ssh_tunnel_manager: ISshTunnelManager):
        super().__init__(ssh_tunnel_manager)

        self.repository_factory = RepositoryFactory(self.db)
        self.repos = self.repository_factory.generate_repositories(self.__class__)

        self.db.bind(provider='mysql', host='127.0.0.1', user='RudolfJelin', passwd=secret.db_password,
                     db='RudolfJelin$default', port=self.tunnel_manager.get_port())

        dprint("Bound DB")

        self.db.generate_mapping(create_tables=False)
        dprint("Generated mapping")
        set_sql_debug(debug_mode)

    def __del__(self):
        self.db.disconnect()
        self.db.provider = None
        self.db.schema = None

        super().__del__()

    @db_session
    def get_all_instances(self, entity_str: str):
        return self.repos[entity_str].get_all()

    @db_session
    def get_instances(self, entity_str, filter_func, max_count=1000000):
        return self.repos[entity_str].get(filter_func, max_count)

    @db_session
    def get_instance_by_id(self, entity_str, obj_id):
        return self.repos[entity_str].get_by_id(obj_id)

    @db_session
    def has_id(self, entity_str, obj_id):
        return self.repos[entity_str].has_id(obj_id)

    @db_session
    def new_instance(self, entity_str, **kwargs):
        return self.repos[entity_str].create(**kwargs)

    @db_session
    def del_instance(self, entity_str, obj_id):
        return self.repos[entity_str].delete(obj_id)

    @db_session
    def edit_instance_by_id(self, entity_str, obj_id, **kwargs):
        return self.repos[entity_str].edit_by_id(obj_id, **kwargs)
