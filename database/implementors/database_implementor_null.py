from typing import Callable

from database.bridge_database import IDatabaseImplementor


class NullDatabase(IDatabaseImplementor):
    """Null object to test function of system on empty database"""
    # TODO

    def __init__(self):
        pass

    def get_all_instances(self, entity_str: str) -> list:
        return []

    def get_instances(self, entity_str: str, filter_func: Callable[[any], bool], max_count: int):
        return []

    def get_instance_by_id(self, entity_str: str, obj_id: int):
        return None

    def has_id(self, entity_str: str, obj_id: int) -> bool:
        return False

    def new_instance(self, entity_str: str, **kwargs):
        return None

    def del_instance(self, entity_str: str, obj_id: int) -> bool | None:
        return None
