from abc import ABC, abstractmethod
from typing import Callable

from database.ssh_tunnel import ISshTunnelManager


class IDatabaseImplementor(ABC):
    """
    An abstract interface common for all possible databases.
    Used solely by IDatabase's implementations.
    Defines methods common to all possible databases
    """

    db: any = None

    def __init__(self, ssh_tunnel_manager: ISshTunnelManager):
        self.tunnel_manager = ssh_tunnel_manager
        self.tunnel_manager.connect()

    def __del__(self):
        self.db = None
        self.tunnel_manager.disconnect()

    @abstractmethod
    def get_all_instances(self, entity_str: str) -> list:
        """Get a list of all instances"""

    @abstractmethod
    def get_instances(self, entity_str: str, filter_func: Callable[[any], bool], max_count: int):
        """Get a list of all instances"""

    @abstractmethod
    def get_instance_by_id(self, entity_str: str, obj_id: int):
        """Get single instance with ID"""

    @abstractmethod
    def has_id(self, entity_str: str, obj_id: int) -> bool:
        """Check if an ID is present, return bool"""

    @abstractmethod
    def new_instance(self, entity_str: str, **kwargs):
        """Create a new instance"""

    @abstractmethod
    def del_instance(self, entity_str: str, obj_id: int) -> bool:
        """Delete an instance"""

    @abstractmethod
    def edit_instance_by_id(self, entity_str: str, obj_id: int, **kwargs) -> bool:
        """Edit an instance with a list of params. Returns False if not found"""


class IDatabaseOperator(ABC):
    """
    An abstract interface for high-level database access.
    Defines methods common to all possible database abstraction APIs
    """

    def __init__(self, database_implementor):
        self.database_implementor = database_implementor

    # A reference to an IDatabaseImplementor implementation
    database_implementor: IDatabaseImplementor

    # todo: define high-level abstract methods for simple usage.
