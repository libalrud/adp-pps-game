from database.bridge_database import IDatabaseOperator, IDatabaseImplementor


class DatabaseBuilder(IDatabaseOperator):
    """Useful for creating objects ??? TODO"""

    def __init__(self, database_implementor: IDatabaseImplementor):
        super().__init__(database_implementor)

    # todo: implement any abstract methods from IDatabaseOperator
    # these should call the IDatabaseImplementor methods implemented in MySQLDatabase
