from database.bridge_database import IDatabaseOperator, IDatabaseImplementor


class DatabaseOperator(IDatabaseOperator):
    """Provides an implementation for the high-level API specified by IDatabaseImplementor"""

    def __init__(self, database_implementor: IDatabaseImplementor):
        super().__init__(database_implementor)

    # TODO: add functions as needed
    # thats all
