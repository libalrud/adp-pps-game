from abc import ABC, abstractmethod
import sshtunnel
import secret
from utilities.debug_mode import dprint


class ISshTunnelManager(ABC):
    """An interface for SSH tunnel managers"""

    class TunnelNotActiveException(Exception):
        """Raised if tunnel not active but accessed"""
        pass

    def __del__(self):
        self.disconnect()

    @abstractmethod
    def connect(self):
        """Start the SSH connection"""

    @abstractmethod
    def disconnect(self):
        """End the SSH connection"""

    @abstractmethod
    def get_port(self):
        """Return current bound port, if active"""


class SshTunnelManager(ISshTunnelManager):
    """Implementation of the above"""

    def __init__(self):

        sshtunnel.SSH_TIMEOUT = 8.0
        sshtunnel.TUNNEL_TIMEOUT = 8.0

        self.tunnel = sshtunnel.SSHTunnelForwarder(
            ssh_address_or_host=('ssh.pythonanywhere.com', 22),
            ssh_username='RudolfJelin', ssh_password=secret.ssh_password,
            remote_bind_address=('RudolfJelin.mysql.pythonanywhere-services.com', 3306))

        self.active = False

    def connect(self):
        if self.active:
            return

        self.tunnel.start()
        self.active = True

        dprint("SSH tunnel start")

    def disconnect(self):
        if not self.active:
            return

        self.tunnel.stop()
        self.active = False
        dprint("SSH tunnel end")

    def get_port(self):
        if not self.active:
            raise ISshTunnelManager.TunnelNotActiveException

        return self.tunnel.local_bind_port
