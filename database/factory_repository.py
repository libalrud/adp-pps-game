from pony.orm import count, select
from typing import Callable
import inspect


class RepositoryFactory:
    """Object facilitating the creation of repository objects for the database"""

    def __init__(self, db):
        self.db = db
        self.repos = {}

    def create_repository(self, entity: type):
        """Creates a new database table wrapper object. TODO move more code here"""
        new_repo = BaseRepository(self.db, entity)

        self.repos[entity.__name__.lower()] = new_repo
        return new_repo

    def get_repositories(self):
        """Get all repositories as a dict"""
        return self.repos

    def generate_repositories(self, containing_class: type):
        """Generate repos for class"""
        classes = self._get_nested_classes(containing_class)
        for cls in classes:
            self.create_repository(cls[1])

        return self.repos

    @staticmethod
    def _get_nested_classes(containing_class):
        def predicate(member):
            return inspect.isclass(member) and member.__module__ == containing_class.__module__

        clss = inspect.getmembers(containing_class, predicate)
        return clss


class BaseRepository:
    """
    Defines an interface for generic database accessing
    Helper object for database implementors! All methods need to be called with db_session scope
    """

    def __init__(self, db, entity):
        """Construct with a database object and an entiti object"""
        self.db = db
        self.entity = entity

    def get_all(self):
        """Return a list of all instances of the given entity type"""
        return self.entity.select()[:]

    def get(self, filter_func: Callable[[any], bool], max_count: int):
        """Get up to max_count instances of the given type for which filter_func is true."""
        return select(instance for instance in self.entity if filter_func(instance))[:max_count]

    def has_id(self, obj_id: int):
        """Return bool existence of object"""
        return self._check_id(obj_id)

    def get_by_id(self, obj_id: int):
        """Return object with given ID or None if none"""
        # todo null object
        if not self._check_id(obj_id):
            return None

        return self.entity[obj_id]

    def create(self, **kwargs):
        """Adds an instance with given params"""
        return self.entity(**kwargs)

    def delete(self, obj_id: int):
        """Deletes object if exists. Returns true iff existed."""

        if not self._check_id(obj_id):
            return False

        obj = self.get_by_id(obj_id)
        if obj:
            obj.delete()
            return True
        return False

    def edit_by_id(self, obj_id: int, **kwargs):
        """Finds an object by ID and edits it"""

        obj = self.get_by_id(obj_id)

        if not obj:
            return False

        for key, value in kwargs.items():
            if hasattr(obj, key):
                setattr(obj, key, value)

        return True


    def _check_id(self, obj_id: int):
        """Checks if a given ID is valid"""
        if obj_id < 0:
            return False

        return True
