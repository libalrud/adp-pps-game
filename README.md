## Design pattern overview:

### Singleton

implemented as a metaclass at /utils/singleton.py  
uses: the WSGI entry point

### Bridge

abstract "implementation class" and "interface class" at database/bridge_database.py  
implementation classes at: 
- database/implementors/


interface classes at:
- database/operators/

### Factory

creator object for Repository instances replaces the need 
to create additional derived classes for each database table

- database/factory_repository.py

De facto also how BaseRouter manages its own 
handler instances within its own handler container instances

- request_router.py

### Repository

I don't know if this counts but 

- database/factory_repository.py


### Dependency injection

Database implementor object gets passed an instance of ssh tunnel manager

- database/ssh_tunnel.py

### Null object

Not really used properly yet. 

- request_routing/basic_responses.py -- NullResponse
- database/implementors/database_implementor_null.py

### Mock object

An empty database bridge implementor object for testing.

- (database/implementors/database_implementor_mockup.py)

(mocking is also used in tests/input_tests.py) to mock a WSGI request

### Builder

An alternative database operator object

- database/operators/database_builder.py


### Chain of responsibility

The "router" design pattern is a subset of this one. 
Registered handlers are iterated through until 
one returns a proper response

- request_router.py


### Template method

The chain of responsibility matching pattern 
within BaseHandler is overridable and overriden
in StandardHandler while execute is left to the client

- request_handlers/handlers.py


### Command

A command is used to formalize requests 
and abstract over WSGI parameters, 
thus improving readability and DRY principle. 

- request_routing/request_command.py
- (technically also) request_routing/request_response.py

### Facade

This is mainly a "de jure" characterization -- 
the RequestCommand and related components also reduce
the WSGI functionality to a simple set of "in" and "out" params



### Tests

- tests/


Viz [Wiki](https://gitlab.fit.cvut.cz/libalrud/adp-pps-game/-/wikis/home)

<!-- ## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->
