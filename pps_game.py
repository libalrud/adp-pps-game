from request_handlers.debug_handler import DebugHandler
from request_handlers.not_found_handler import NotFoundHandler
from request_routing.request_command import RequestCommand
from request_routing.request_router import Router

from utilities.singleton import Singleton


class PpsGame(Singleton):
    """
    Singleton class handling requests through wsgi
    https://wsgi.readthedocs.io/en/latest/definitions.html
    """

    def __init__(self):
        self.router = Router()
        self.add_routes()

    def add_routes(self):
        """
        Add routes to be used in the app here
        Create a class derived from BaseHandler or StandardHandler
        """
        # todo router.add stuff

        # 1) debug
        self.router.add_route(DebugHandler)

        # 2) homepage

        # 3) everything else

        # 4) fallback = 404
        self.router.add_route(NotFoundHandler)

    def process_request(self, environ) -> tuple[str, str, bytes]:
        """Entry point for all requests"""

        request_command = RequestCommand(environ=environ)

        result = self.router.handle_request(request_command)
        return result.status, result.content_type, result.content
