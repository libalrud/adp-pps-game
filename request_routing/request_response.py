from abc import ABC
from http import HTTPStatus


class RequestResponse(ABC):
    """
    Simple encapsulation of server responses to improve readability, abstraction and logging
    Use this instead of passing back status code, content type and content
    """

    def __init__(self, http_status: HTTPStatus, mime_type: str, charset: str | None, content: str | bytes):
        """Default Constructor does nothing"""
        # TODO: compute result from some params
        # TODO: encapsulate things like mime type, content type, etc etc

        self.status = self.get_status_string(http_status)
        self.content_type = self.get_content_type(mime_type, charset)
        self.content = self.get_content(content, charset)

    @staticmethod
    def get_content(content: str | bytes, charset: str | None):
        if charset is None or charset == 'binary':
            result = content
        else:
            result = content.encode(encoding=charset)

        if not isinstance(result, bytes):
            raise TypeError("Result sent back is not binary data or encoded text!")

        return result

    @staticmethod
    def get_content_type(mime_type: str, charset: str | None):
        if charset is None or charset == 'binary':
            return f"{mime_type}"
        else:
            return f"{mime_type}; {charset=}"

    @staticmethod
    def get_status_string(http_status: HTTPStatus):
        return f"{http_status.value} {http_status.phrase}"

# text/html; charset=UTF-8
# image/x-icon
# image/jpeg
# text/csv
# application/vnd.android.package-archive
# application/zip
# application/json; charset=UTF-8
