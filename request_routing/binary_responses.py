from abc import ABC
from http import HTTPStatus

from request_routing.request_response import RequestResponse


class BinaryResponse(ABC, RequestResponse):
    """Generic response for all binary data"""

    def __init__(self, mime_type: str, content: bytes, http_status: HTTPStatus = HTTPStatus.OK):
        super().__init__(http_status=http_status, mime_type=mime_type, charset=None, content=content)


class FaviconResponse(BinaryResponse):
    """Favicon x-icon"""

    def __init__(self, content):
        super().__init__(mime_type="image/x-icon", content=content)


class PngResponse(BinaryResponse):
    """Any PNG image"""

    def __init__(self, content):
        super().__init__(mime_type="image/png", content=content)


class JpegResponse(BinaryResponse):
    """Any JPEG image"""

    def __init__(self, content):
        super().__init__(mime_type="image/jpeg", content=content)


class ApkResponse(BinaryResponse):
    """Any android APK"""

    def __init__(self, content):
        super().__init__(mime_type="application/vnd.android.package-archive", content=content)


class ZipResponse(BinaryResponse):
    """Any ZIP package"""

    def __init__(self, content):
        super().__init__(mime_type="application/zip", content=content)
