from abc import ABC
from http import HTTPStatus

from config import PREFERRED_ENCODING
from request_routing.request_response import RequestResponse


class TextResponse(RequestResponse):
    """Classic text/html response"""

    def __init__(self, content_str: str, mime_type: str = "text/html", http_status=HTTPStatus.OK):
        super().__init__(http_status, mime_type, PREFERRED_ENCODING, content_str)


class JsonResponse(TextResponse):
    """JSON response"""

    def __init__(self, content_str: str):
        super().__init__(content_str=content_str, mime_type="application/json")


class NullResponse(TextResponse):
    """Empty response"""

    def __init__(self):
        super().__init__(content_str="")
