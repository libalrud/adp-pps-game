from abc import ABC

from request_routing.request_command import RequestCommand
from request_routing.request_response import RequestResponse


class HandlerException(Exception):
    """
    Thrown if a handler unexpectedly fails at its task
    Throw this to signalize "I thought I could do this, but I failed"
    """


class NoMatchedHandleException(Exception):
    """Thrown if no handler is appropriate"""


class HandlerContainer:
    """Encapsulates the handler functionality"""

    def __init__(self, handler_type):
        self.handler_type = handler_type
        self.handler_instance = handler_type()  # instantiate and keep reference

    def matches(self, path: str):
        return self.handler_instance.match(path)

    def execute(self, command: RequestCommand) -> RequestResponse:
        return self.handler_instance.execute(command)


class BaseRouter(ABC):
    """
    Chain of responsibility structure to send requests on their way
    idea: add_route should instantiate and keep instance for itself and automatically use it when selected
    the overriding object should have a method that discerns if it handles the path given
    """

    def __init__(self):
        self.handlers = []

    def add_route(self, handler_type):
        """Add an option to the router"""
        self.handlers.append(HandlerContainer(handler_type))  # factory 100

    def handle_request(self, command: RequestCommand) -> RequestResponse:
        """Iterate over handlers until one of them returns successfully. None if none."""
        failed_handlers = []

        for handler in self.handlers:
            if handler.matches(command.path_request):
                try:
                    return handler.execute(command)
                except HandlerException:
                    failed_handlers.append(handler.__doc__)

        raise NoMatchedHandleException(f"No suitable request handler! Tried: {failed_handlers}")


class Router(BaseRouter):
    """Implements a BaseRouter override"""

    def __init__(self):
        super().__init__()

    # todo ?
