import datetime
from urllib.parse import parse_qs
from config import URL_PREFIX


class RequestCommand:
    """Simple encapsulation of server requests to improve readability and logging"""

    def __init__(self, environ):
        """Automatically extracts important information"""

        self.request_method = environ['REQUEST_METHOD']
        self.path_info = environ['PATH_INFO']
        self.path_request = self.path_info[len(URL_PREFIX):]
        self.query_info = parse_qs(environ['QUERY_STRING'])

        self.datetime = datetime.datetime.now()

        # todo something for post requests

        # TODO logging

    def __str__(self):
        return f"Request at time {self.datetime}: {self.request_method=}, {self.path_request=}, {self.query_info=}"

