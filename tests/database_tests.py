import unittest

from database.implementors.database_implementor_mysql import MySQLDatabase
from database.operators.database_operator import DatabaseOperator
from database.ssh_tunnel import SshTunnelManager


class DatabaseTests(unittest.TestCase):
    """A set of tests of the database control and the associated bridge pattern superstructure"""

    def test_implementation(self):
        """
        If all abstract methods have been implemented
        """

        database_implementor = MySQLDatabase(ssh_tunnel_manager=SshTunnelManager())
        database_operator = DatabaseOperator(database_implementor=database_implementor)

        # that's all
        del database_implementor
        del database_operator

    def test_nonexistent_user(self):
        """Fetching nonexistent user returns nothing"""

        database_implementor = MySQLDatabase(ssh_tunnel_manager=SshTunnelManager())

        users_with_name = [u for u in database_implementor.get_all_instances("user") if u.name == "jhsudajskldnsgds"]

        users_with_name_func = database_implementor.get_instances("user", lambda u: u.name == "jhsudajskldnsgds")

        self.assertEquals(len(users_with_name), 0)
        self.assertEquals(len(users_with_name_func), 0)

        del database_implementor

    def test_add_remove_user(self):
        """Add and remove a user"""

        db_impl = MySQLDatabase(ssh_tunnel_manager=SshTunnelManager())

        user_count = len([u for u in db_impl.get_all_instances("user")])

        ua = db_impl.new_instance("user", name="test_add_remove_user_test", password=bytes(2))

        user_count_2 = len([u for u in db_impl.get_all_instances("user")])

        self.assertIsNotNone(ua)

        self.assertEquals(user_count + 1, user_count_2)

        deleted = db_impl.del_instance("user", ua.id)

        user_count_3 = len([u for u in db_impl.get_all_instances("user")])

        self.assertEquals(deleted, True)

        self.assertEquals(user_count_2 - 1, user_count_3)

        del db_impl

    def test_user_search(self):
        """Creates a user, edits, deletes"""

        db_impl = MySQLDatabase(ssh_tunnel_manager=SshTunnelManager())

        try:
            usr1 = db_impl.new_instance("user", name="test_add_remove_user_test_2", password=bytes(2))
        except Exception:
            usr1_list = db_impl.get_instances("user", lambda u: u.name == "test_add_remove_user_test_2")
        else:

            usr1_list = db_impl.get_instances("user", lambda u: u.name == "test_add_remove_user_test_2")

            # only one user with the name
            self.assertEquals(len(usr1_list), 1)

            # same name
            self.assertEquals(usr1.name, usr1_list[0].name)

        deleted = db_impl.del_instance("user", usr1_list[0].id)
        self.assertEquals(deleted, True)

        del db_impl

    def test_cipher_user_link(self):
        """Create cipher, user, test link, then remove them"""

        db_impl = MySQLDatabase(ssh_tunnel_manager=SshTunnelManager())

        user = db_impl.new_instance("user", name="test_link_user_test", password=bytes(2))

        cipher = db_impl.new_instance("cipher", name="test_cipher_test", operations="None_test", ownerID=user.id)

        # user_with_cipher = db_impl.get
        # TODO


