import unittest

from pps_game import PpsGame


class InputTests(unittest.TestCase):
    """A set of tests covering base use cases of the request handler"""

    @staticmethod
    def mock_environ(path: str, post: bool = False):
        environ = {'REQUEST_METHOD': "GET" if not post else "POST",
                   'PATH_INFO': path.split('?')[0],
                   'QUERY_STRING': "" if "?" not in path else path.split('?')[1]}

        return environ

    def test_init(self):
        """
        Tests if initialization and fallback method works
        """

        pps = PpsGame()

        result = pps.process_request(environ=self.mock_environ("/adp/test_test/?v=4&v=3&w=asdf"))

        print(result)

        self.assertIsNotNone(result)

    def test_404(self):
        """
        Tests if 404 works
        """

        pps = PpsGame()

        result = pps.process_request(environ=self.mock_environ("/adp/jdioasoidhasdjknasjido?s=sdjsaiojd"))

        print(result)

        self.assertEqual(result[0], "404 Not Found")

    def test_debug(self):
        """
        Tests if debug works
        """

        pps = PpsGame()

        result = pps.process_request(environ=self.mock_environ("/adp/debug/"))

        print(result)
        self.assertEqual(result[0], "200 OK")





