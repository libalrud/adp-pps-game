from pony import orm
import MySQLdb
import sshtunnel


def test1():
    db = orm.Database()
    db.bind(provider='mysql', host='RudolfJelin.mysql.pythonanywhere-services.com',
            user='RudolfJelin', passwd='Quijibo11', db='default')


def test2():
    sshtunnel.SSH_TIMEOUT = 5.0
    sshtunnel.TUNNEL_TIMEOUT = 5.0

    with sshtunnel.SSHTunnelForwarder(
            ('ssh.pythonanywhere.com', 22),
            ssh_username='RudolfJelin',
            ssh_password='Quijibo11',
            remote_bind_address=(
                    'RudolfJelin.mysql.pythonanywhere-services.com', 3306)
    ) as tunnel:
        connection = MySQLdb.connect(
            user='RudolfJelin',
            passwd="m6:b'E'UWz#wez_",
            host='127.0.0.1', port=tunnel.local_bind_port,
            db='RudolfJelin$default',
        )
        # Do stuff
        connection.close()


def test3():
    orm.set_sql_debug(True)
    sshtunnel.SSH_TIMEOUT = 5.0
    sshtunnel.TUNNEL_TIMEOUT = 5.0

    with sshtunnel.SSHTunnelForwarder(
            ('ssh.pythonanywhere.com', 22),
            ssh_username='RudolfJelin',
            ssh_password='Quijibo11',
            remote_bind_address=(
                    'RudolfJelin.mysql.pythonanywhere-services.com', 3306)
    ) as tunnel:
        db = orm.Database()
        db.bind(provider='mysql', host='127.0.0.1',
                user='RudolfJelin', passwd="m6:b'E'UWz#wez_", db='RudolfJelin$default', port=tunnel.local_bind_port)


if __name__ == "__main__":
    test3()
